/**
 * BÀI1: TÍNH TIỀN LƯƠNG NHÂN VIÊN
 * input: lương 1 ngày và số ngày làm
 * output:
 */

var luongMotngay = 100000;
var soNgaylam = 5;
var luong;

luong = luongMotngay * soNgaylam;
console.log("luong", luong);

/**
 * BÀI 2: TÍNH GIÁ TRỊ TRUNG BÌNH
 */

var Num1 = 1;
var Num2 = 2;
var Num3 = 3;
var Num4 = 4;
var Num5 = 5;
var soTrungbinh;
soTrungbinh = (Num1+Num2+Num3+Num4+Num5) / 5;

console.log('sotrungbinh', soTrungbinh);


/**
 * BÀI 3: QUY ĐỔI TIỀN
 */

var giaUsd = 23500;
var soTienUsd = 5000;
var giaTriVnd;
giaTriVnd = giaUsd * soTienUsd;

console.log('gia tri VND', giaTriVnd);

/**
 * BÀI 4: TÍNH DIỆN TÍCH - CHU VI HÌNH CHỮ NHẬT
 */

var chieuDai = 5;
var chieuRong = 4;
var dienTich;
var chuVi;
dienTich = chieuDai * chieuRong;
chuVi = (chieuDai + chieuRong)*2;
console.log('dien tich', dienTich);
console.log('chu vi', chuVi);


/**
 * BÀI 5: TÍNH TỔNG 2 KÍ SỐ
 */

var Num = 35;
var soHangDonVi = Num % 10;
var soHangChuc = Math.floor(Num/10);
var result = soHangChuc + soHangDonVi;
console.log('tong ky so', result);